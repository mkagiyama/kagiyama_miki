package kagiyama_miki.service;

import static kagiyama_miki.utils.CloseableUtil.*;
import static kagiyama_miki.utils.DBUtil.*;

import java.sql.Connection;

import kagiyama_miki.beans.User;
import kagiyama_miki.dao.UserDao;
import kagiyama_miki.utils.CipherUtil;


public class UserService {

    public void register(User user) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		String encPassword1 = CipherUtil.encrypt(user.getPassword1());
    		user.setPassword1(encPassword1);

    		UserDao userDao = new UserDao();
    		userDao.insert(connection, user);

    		commit(connection);
    	} catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch(Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void update(User user) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		String encPassword = CipherUtil.encrypt(user.getPassword1());
    		user.setPassword1(encPassword);

    		UserDao userDao = new UserDao();
    		userDao.update(connection, user);

    		commit(connection);
    	} catch (RuntimeException e) {
    		rollback(connection);
    		System.out.println("エラーだお");
    	} catch(Error e) {
    		rollback(connection);
    	} finally {
    		close(connection);
    	}
    }
}
