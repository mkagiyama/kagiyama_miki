package kagiyama_miki.service;

import static kagiyama_miki.utils.CloseableUtil.*;
import static kagiyama_miki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import kagiyama_miki.beans.Member;
import kagiyama_miki.dao.MemberDao;



public class MemberService extends HttpServlet{

	public List<Member> getMember() {

		Connection connection = null ;
		try {
			connection = getConnection();

			MemberDao memberDao = new MemberDao();
			List<Member> ret = memberDao.getMembers(connection);

			commit(connection);

			return ret ;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Member getLoginId(String value) {

		Connection connection = null ;
		try {
			connection = getConnection();

			MemberDao memberDao = new MemberDao();
			Member member = memberDao.getLoginId(connection, value);

			commit(connection);

			return member ;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Member getEditUser(int userId) {
		Connection connection = null;
		try {
			connection = getConnection();

			MemberDao memberDao = new MemberDao();
			Member member = memberDao.getEditUser(connection, userId);

			commit(connection);

			return member;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}