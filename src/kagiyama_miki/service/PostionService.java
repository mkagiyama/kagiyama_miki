package kagiyama_miki.service;

import static kagiyama_miki.utils.CloseableUtil.*;
import static kagiyama_miki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import kagiyama_miki.beans.Position;
import kagiyama_miki.dao.PositionDao;


public class PostionService extends HttpServlet {
	public List<Position> getPosition() {
		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Position> ret= positionDao.getPositionNames(connection);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
