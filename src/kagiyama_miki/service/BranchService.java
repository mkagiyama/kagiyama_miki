package kagiyama_miki.service;

import static kagiyama_miki.utils.CloseableUtil.*;
import static kagiyama_miki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import kagiyama_miki.beans.Branch;
import kagiyama_miki.dao.BranchDao;


public class BranchService extends HttpServlet {

	public List<Branch> getBranch(){

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret = branchDao.getBranchNames(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
