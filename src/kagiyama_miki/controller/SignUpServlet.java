package kagiyama_miki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kagiyama_miki.beans.Branch;
import kagiyama_miki.beans.Member;
import kagiyama_miki.beans.Position;
import kagiyama_miki.beans.User;
import kagiyama_miki.service.BranchService;
import kagiyama_miki.service.MemberService;
import kagiyama_miki.service.PostionService;
import kagiyama_miki.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Branch>  branchList = new BranchService().getBranch();
		request.setAttribute("branch", branchList);

		List<Position> positionList = new PostionService().getPosition();
		request.setAttribute("position", positionList);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		User editUser = getEditUser(request);
		List<Branch>  branchList = new BranchService().getBranch();
//		request.setAttribute("branch", branchList);
		List<Position> positionList = new PostionService().getPosition();
//		request.setAttribute("position", positionList);


		if(isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setName(request.getParameter("name"));
			user.setPassword1(request.getParameter("password1"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
			user.setPositionId(Integer.parseInt(request.getParameter("position_id")));

			new UserService().register(user);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
    		request.setAttribute("branch", branchList);
    		request.setAttribute("position", positionList);
    		request.getRequestDispatcher("signup.jsp").forward(request, response);
//			response.sendRedirect("signup");
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException,ServletException{

		User editUser = new User();
//		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword1(request.getParameter("password1"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("position_id")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");

		Member member = new MemberService().getLoginId(loginId);

		if(StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}
		if(!loginId.matches("^[a-zA-z0-9]{6,20}")) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下です。");
		}
		if(member != null) {
			messages.add("そのアカウントは存在しています。");
		}
		if(StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}

		if(StringUtils.isEmpty(password1) == true) {
			messages.add("パスワードを入力してください");
		}
		if(!password1.matches("^[!-~]{6,20}")) {
			messages.add("パスワードは記号を含む半角英数字6文字以上20時以下です。");
		}

		if(!password2.equals(password1)) {
			messages.add("パスワードが不一致です。");
		}

		if(name.length() > 10) {
			messages.add("ユーザー名は10文字以下で入力してください。");
		}

		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
