package kagiyama_miki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kagiyama_miki.beans.Member;
import kagiyama_miki.service.MemberService;



@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

 @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	 List<Member> member = new MemberService().getMember();
	 request.setAttribute("member", member);

	 request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

}
