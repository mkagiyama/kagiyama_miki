package kagiyama_miki.dao;

import static kagiyama_miki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kagiyama_miki.beans.Position;
import kagiyama_miki.exception.SQLRuntimeException;


public class PositionDao {
	public List<Position> getPositionNames(Connection connection){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("position.id as id,");
			sql.append("position.name as name ");
			sql.append("FROM position");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositionList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally{
			close(ps);
		}
	}

	private List<Position> toPositionList(ResultSet rs)
			throws SQLException{

		List<Position> ret = new ArrayList<Position>();
		try {
			while(rs.next()) {
				String positionName = rs.getString("name");
				int id = rs.getInt("id");

				Position positionList = new Position();
				positionList.setPositionName(positionName);
				positionList.setId(id);

				ret.add(positionList);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
