package kagiyama_miki.dao;

import static kagiyama_miki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kagiyama_miki.beans.Branch;
import kagiyama_miki.exception.SQLRuntimeException;


public class BranchDao {
	public List<Branch> getBranchNames(Connection connection){

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("branch.id as id,");
			sql.append("branch.name as name ");
			sql.append("FROM branch ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret ;

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs)
			throws SQLException{

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while(rs.next()){
				String branchName = rs.getString("name");
				int id = rs.getInt("id");

				Branch branchList = new Branch();
				branchList.setBranchName(branchName);
				branchList.setId(id);

				ret.add(branchList);
			}
			return ret;
		} finally {
		close(rs);
		}
	}
}
