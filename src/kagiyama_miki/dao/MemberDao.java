package kagiyama_miki.dao;

import static kagiyama_miki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;

import kagiyama_miki.beans.Member;
import kagiyama_miki.exception.SQLRuntimeException;

public class MemberDao extends HttpServlet {

	public List<Member> getMembers(Connection connection){

		PreparedStatement ps = null ;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("branch.name as branch_name, ");
			sql.append("position.name as position_name ");
			//sql.append("users.created_date as created_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branch ");
			sql.append("ON users.branch_id = branch.id ");
			sql.append("INNER JOIN position ");
			sql.append("ON users.position_id = position.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Member> ret = toMemberList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Member> toMemberList(ResultSet rs)
			throws SQLException{

		List<Member> ret = new ArrayList<Member>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");
				//Timestamp createdDate = rs.getTimestamp("created_date");

				Member member = new Member();
				member.setId(id);
				member.setLoginId(loginId);
				member.setPassword(password);
				member.setName(name);
				member.setBranchName(branchName);
				member.setPositionName(positionName);

				ret.add(member);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public Member getLoginId(Connection connection, String loginId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("users.login_id as login_id ");
			sql.append("FROM users ");
			sql.append("WHERE users.login_id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			List<Member> memberList = toMemberLoginIdList(rs);
			if(memberList.isEmpty() == true) {
				return null;
			} else if (2 <= memberList.size()) {
				throw new IllegalStateException("2 <= memberList.size()");
			} else {
				return memberList.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Member> toMemberLoginIdList(ResultSet rs) throws SQLException{

		List<Member> ret = new ArrayList<Member>();
		try {
			while(rs.next()) {
				String loginId = rs.getString("login_id");

				Member member = new Member();
				member.setLoginId(loginId);

				ret.add(member);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public Member getEditUser(Connection connection , int userId) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.password as password, ");
			sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id ");
			sql.append("FROM users ");
/*			sql.append("INNER JOIN branch ");
			sql.append("ON users.branch_id = branch.id ");
			sql.append("INNER JOIN position ");
			sql.append("ON users.position_id = position.id ");
*/
			sql.append("WHERE users.id = ? ");


            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1,userId);

            ResultSet rs = ps.executeQuery();
			List<Member> memberList = toMemberEditUserList(rs);

			if(memberList.isEmpty() == true) {
				return null;
			} else if (2 <= memberList.size()) {
				throw new IllegalStateException("2 <= memberList.size()");
			} else {
				return memberList.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Member> toMemberEditUserList(ResultSet rs) throws SQLException{

		List<Member> ret = new ArrayList<Member>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchName = rs.getInt("branch_id");
				int positionName = rs.getInt("position_id");

				Member member = new Member();
				member.setId(id);
				member.setLoginId(loginId);
				member.setPassword(password);
				member.setName(name);
				member.setBranchId(branchName);
				member.setPositionId(positionName);

				ret.add(member);
			}
			return ret;
		} finally {
			 close(rs);
		}

	}
}
