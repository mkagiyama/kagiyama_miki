<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザー登録</title>
		</head>

		<body>
			<div class="main-contents">
				<c:if test= "${not empty errorMessages }" >
					<div class = "errorMessages">
						<ul>
							<c:forEach items ="${errorMessages}" var = "message" >
								<li><c:out value = "${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var = "errorMessages" scope = "session" />
				</c:if>

				<form action = "signup" method = "post" >

					<br /> <label for = "login_id">ログインID </label>
					<input name = "login_id"  value="${ editUser.loginId }" id = "login_id" /><br />
					<label for = "password1">パスワード</label>
					<input name = "password1" type = "password" id = "password1" /><br />
					<label for = "password2">確認用パスワード</label>
					<input name = "password2" type = "password" id = "password2" /><br />
					<label for = "name" >ユーザー名</label>
					<input name = "name" value="${ editUser.name }" id = "name" /><br />

				 	<label for = "branch">支店</label>
				 	<select name = "branch_id" id = "branch_id">
					<c:forEach items="${branch}" var="br">
					<c:if test = "${ br.id == editUser.branchId }">
					<option value="${ br.id }" selected>"${ br.branchName }" </option>
					</c:if>
					<c:if test = "${ br.id != editUser.branchId }">
					<option value="${ br.id }" >"${ br.branchName }" </option>
					</c:if>
					</c:forEach>
					</select><br />

				 	<label for = "position">部署・役職</label>
				 	<select name = "position_id" id = "position_id">
					<c:forEach items="${position}" var="po">
					<c:if test = "${ po.id == editUser.positionId }">
					<option value="${ po.id }" selected >"${ po.positionName }" </option>
					</c:if>
					<c:if test = "${ po.id != editUser.positionId }">
					<option value="${ po.id }" >"${ po.positionName }" </option>
					</c:if>
					</c:forEach>
					</select><br />

					<input type = "submit" value = "登録" /><br />
					<a href = "./">戻る</a>

				</form>
			</div>
		</body>
</html>