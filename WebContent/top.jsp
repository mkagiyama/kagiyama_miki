<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
	<head>
		<meta charset="UTF-8">
			<title>掲示板システム</title>
	</head>

	<body>
		<div class = "main-contents">
			<div class = "header">
				<a href="signup">新規登録</a>
			</div>
		</div>

		<div class = "members">
			<c:forEach items = "${member}" var="member">
				<div class = "member">
					<span class = "login_id"><c:out value = "${member.loginId}" /></span>
					<span class = "name"><c:out value = "${member.name}" /></span>
					<span class = "branch_name"><c:out value = "${member.branchName}" /></span>
					<span class = "position_name"><c:out value = "${member.positionName}" /></span>

					<form action = "settings" method = "get">
					<input name = "id" id = "id"  value = "${ member.id }" type = "hidden"/>
					<input type = "submit" value="登録情報更新" />
					</form>
				</div>
			</c:forEach>
		</div>
	</body>
</html>